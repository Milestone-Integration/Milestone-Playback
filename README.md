# Milestone Playback



## Arugments for Debug Mode in Visual Studio

[Milestone Device IP] [Milestone Device Username] [Milestone Device password] [Milestone Device Port] [Milestone Camera Id] [Https Enabled only] [Windows Authentication] [Websocket Port for Output] [Playback Start time Unixtimestamp] [Playback End Time Unixtimestamp] [Playback Forward mode] [Playback Fast Sequence] [Playback Speed]


Example:

192.168.1.32 Admin Sct1234 null fd2f5aa2-8479-4ba3-9dc4-e665b7f1fe98 false true 10005 1679457132312 null false false 0


[![Watch the video](https://solutionchamps.com/thumbnail/video-cover.png)](https://gitlab.com/Milestone-Integration/Milestone-Playback/-/blob/main/Milestone_Playback.webm)
