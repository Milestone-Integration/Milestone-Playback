﻿using MSLocalModeLite.Objects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Threading;
using VideoOS.Platform;
using VideoOS.Platform.Data;
using VideoOS.Platform.Messaging;
using VideoOS.Platform.SDK;
using System.Collections.Specialized;
using WebSocketSharp.Server;
using System.Timers;

namespace MSLocalModeLite
{
   public class Playback: WebSocketBehavior
    {
        uint plybckframeCount = 0;
        public Queue queue = new Queue();
        List<ActionListDetails> actionList = new List<ActionListDetails>();
        private System.Timers.Timer timer;
        

        static Playback() {
            VideoOS.Platform.SDK.Environment.Initialize();
            VideoOS.Platform.SDK.Media.Environment.Initialize();
            VideoOS.Platform.SDK.Export.Environment.Initialize();
        }
      
       public class ActionListDetails
        {
            public string StreamID { get; set; }
            public bool isForward { get; set; }
            public bool isPlay { get; set; }
            public bool fastseq { get; set; }
            public double Speed { get; set; }

        }


        /********************************************************************************************
           * Description: Playback Stream request overloaded
           * Last Update: 01-01-2023
           * Updater: Rekha Murugesan
           * License: NXG
           ********************************************************************************************/
        public string StreamPlayback(RequestParameters requestParam)
        {
            try
            {
               
                bool status = false;
                NetworkCredential credential = new NetworkCredential(requestParam.DeviceUser, requestParam.DevicePassword);
                int port;
                if (!int.TryParse(requestParam.DeviceHttpPort, out port))
                {
                    port = requestParam.HttpsEnabled ?? false ? 443 : 80;
                }
                Uri uri = new UriBuilder() { Host = (requestParam.DeviceIP ?? String.Empty).Trim(), Port = port }.Uri;


                ActionListDetails listDetails = new ActionListDetails();
                listDetails.StreamID = StreamQueueManager.StreamingIdentifier;
                listDetails.isPlay = true;
                listDetails.fastseq = requestParam.isFastseq;
                listDetails.Speed = requestParam.Speed;
                listDetails.isForward = requestParam.isForward;


                // connect

                if ((bool)requestParam.WindowsAuth)
                {
                    VideoOS.Platform.SDK.Environment.AddServer(false, uri, credential);
                }
                else
                {
                    CredentialCache credentials = new CredentialCache();
                    credentials.Add(uri, "Basic", credential);
                    VideoOS.Platform.SDK.Environment.AddServer(false, uri, credentials);
                }
             
                VideoOS.Platform.SDK.Environment.Login(uri, Guid.Parse("8cb5b519-0c25-484b-96ff-2a7ded97cc1c"), "Genesis MileStone Local Mode", "1.0.0", "Genesis", false);
                status = VideoOS.Platform.SDK.Environment.IsLoggedIn(uri);
                if (status)
                {
                    Console.WriteLine("false");
                    Console.WriteLine("Device connected");
                }
                else
                {
                    Console.WriteLine("true");
                    Console.WriteLine("Connection failed:");
                }


                Item itemData1;
                if ((bool)requestParam.WindowsAuth)
                {
                    UserContext userContext = VideoOS.Platform.SDK.MultiEnvironment.CreateSingleServerUserContext(false, requestParam.DeviceUser, requestParam.DevicePassword, true, uri);
                    //bool loginSuceeded = VideoOS.Platform.SDK.MultiEnvironment.LoginUserContext(userContext);
                    itemData1 = VideoOS.Platform.SDK.MultiEnvironment.LoadSiteItem(false, uri, userContext.CredentialCache);
                    MultiEnvironment.Logout(userContext);
                }
                else
                {
                    CredentialCache credentials = new CredentialCache();
                    credentials.Add(uri, "Basic", credential);
                    itemData1 = VideoOS.Platform.SDK.Environment.LoadSiteItem(false, uri, credentials);
                }

                if (itemData1 != null)
                {
                    Item itemData = VideoOS.Platform.Configuration.Instance.GetItem(itemData1.FQID);
                    Item selectedCamera = GetCameraInfo.GetSelectedCamera(requestParam.CameraId, itemData);

                    JPEGVideoSource _jpegVideoSource = new JPEGVideoSource(selectedCamera);
                    _jpegVideoSource.Width = 640;
                    _jpegVideoSource.Height = 360;
                    _jpegVideoSource.Init();
                    TimeSpan ts = requestParam.EndTime.Subtract(requestParam.StartTime);

                    List<object> jpegVideoSourceList = new List<object>();
                    Thread _fetchThread = null;
                    jpegVideoSourceList = _jpegVideoSource.Get(requestParam.StartTime, ts, 1);

                    _fetchThread = new Thread(() => RawFetchThread(StreamQueueManager.StreamingIdentifier, jpegVideoSourceList, _jpegVideoSource, requestParam.StartTime, listDetails));
                    _fetchThread.Start();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Playback error: {0}", ex.Message);
                Console.WriteLine("Playback errorDetails: {0}", ex.StackTrace);
            }
            string json_Final = Newtonsoft.Json.JsonConvert.SerializeObject("not implemented");
            return json_Final;
        }

        /********************************************************************************************
         * Description: Playback Stream get frames from jpegvideosource and ADDqueue for stream indentifier
         * Last Update: 01-01-2023
         * Updater: Rekha Murugesan
         * License: NXG
         ********************************************************************************************/
        public void RawFetchThread(string streamIdentifier, List<object> jpegVideoSourceList, JPEGVideoSource _jpegVideoSource, DateTime startTime, ActionListDetails listDetails)
        {
            JPEGData lastJpegData = null;
            DateTime nextTime = startTime;
            bool isForward = true;
            bool isPlay = true;
           // bool isFastForward = false;
            bool fastseq = false;
            double speed = 0.25;

            try
            {
                if (jpegVideoSourceList != null & jpegVideoSourceList.Count > 0)
                {
                    foreach (JPEGData jpegData in jpegVideoSourceList)
                    {
                        lastJpegData = jpegData;
                        WriteToPlayback(lastJpegData, streamIdentifier);
                    }

                    string socketUrl = "ws://localhost:10005/streaming?streamingid=" + streamIdentifier;
                    Console.WriteLine(socketUrl);
                }
                

                while (lastJpegData != null)
                {

                    isForward = listDetails.isForward;
                    isPlay = listDetails.isPlay;
                    // isFastForward = actionList.FirstOrDefault(x => x.StreamID.Equals(streamIdentifier)).isFastForward;
                    fastseq = listDetails.fastseq;
                    speed = listDetails.Speed;


                    while (!isPlay)
                    {
                        if (actionList.Count > 0)
                        {
                            isPlay = actionList.FirstOrDefault(x => x.StreamID.Equals(streamIdentifier)).isPlay;
                        }
                        int milliseconds = 2000;
                        Thread.Sleep(milliseconds);
                    }

                    if (isForward && speed == 0)
                    {
                        lastJpegData = _jpegVideoSource.GetNext() as JPEGData;
                    }
                    else if (!isForward && speed == 0)
                    {
                        lastJpegData = _jpegVideoSource.GetPrevious();
                    }
                    else
                    {
                        // Reverse or Forward with Speed
                        //stop
                        nextTime = (isForward ? nextTime.AddSeconds(speed) : nextTime.AddSeconds(-1 * speed));
                        //goto
                        _jpegVideoSource.GoTo(nextTime, isForward ? PlaybackPlayModeData.Forward : PlaybackPlayModeData.Reverse);

                        if (fastseq)
                        {
                            if (isForward)
                            {
                                lastJpegData = _jpegVideoSource.GetNextSequence() as JPEGData;
                            }
                            else
                            {
                                lastJpegData = _jpegVideoSource.GetPreviousSequence();
                            }
                        }
                        else
                        {
                            if (isForward)
                            {
                                lastJpegData = _jpegVideoSource.GetNext() as JPEGData;
                            }
                            else
                            {
                                lastJpegData = _jpegVideoSource.GetPrevious();
                            }
                        }
                    }
                    if (lastJpegData != null)
                    {
                        WriteToPlayback(lastJpegData, streamIdentifier);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("RawFetchThread error: {0}", ex.Message);
                Console.WriteLine("RawFetchThread errorDetails: {0}", ex.StackTrace);
            }

        }

        public void WriteToPlayback(JPEGData jpegData, string streamIdentifier)
        {
            plybckframeCount++;
            try
            {
                int nSize = jpegData.Bytes.Length;
                byte[] byteBuf = new byte[nSize];
                if (queue != null)
                {
                    DateTime frameReceiveTime = DateTime.Now;
                    FrameContents fc = new FrameContents(byteBuf, nSize, 360, 640, 25, plybckframeCount, frameReceiveTime);

                    dynamic responseJson = new ExpandoObject();
                    responseJson.buffer = jpegData.Bytes;
                    responseJson.timestamp = 0;
                    string buffer_json = Newtonsoft.Json.JsonConvert.SerializeObject(responseJson);

                    fc.base64Image = buffer_json;
                    bool enqueueStatus = Enqueue(streamIdentifier.ToString(), fc);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Playback error: {0}", ex.Message);
                Console.WriteLine("Playback errorDetails: {0}", ex.StackTrace);
            }
            finally
            {
            }
        }

        internal static bool Enqueue(string identifier, FrameContents fc)
        {
            if (identifier == null)
                return false;

           Queue currentQueue = (Queue)StreamQueueManager.StreamingQueues[identifier];
            if (currentQueue == null)
            {
                currentQueue = new Queue();
                StreamQueueManager.StreamingQueues[identifier] = currentQueue;
            }
            currentQueue.Enqueue(fc);
            return true;
        }

        internal bool Dequeue(string identifier, ref FrameContents fc) {
            if (StreamQueueManager.StreamingIdentifier == null)
                return false;

            Queue currentQueue = (Queue)StreamQueueManager.StreamingQueues[StreamQueueManager.StreamingIdentifier];
            if (currentQueue == null)
            {
                fc = null;
                return false;
            }
            else
            {
                int queueCount = currentQueue.Count;
                if (queueCount > 200)
                {
                    currentQueue.Clear();
                    fc = null;
                }
                else if(queueCount > 0)
                {
                    FrameContents frameObj = (FrameContents)currentQueue.Dequeue();
                    if (frameObj != null && frameObj.base64Image != null)
                        fc = frameObj;
                }
            }
            return true;
        }

        protected override void OnOpen()
        {
            try
            {
                timer = new System.Timers.Timer(); // Initialize the timer for object last access check
                timer.Interval = 10; // Timer Interval as 10 milli seconds
                timer.Elapsed += queuePull; // Callback on Timer elapsed
                timer.Enabled = true; // Enable the timer
                Console.WriteLine("Socket open for Streamingid:" + Context.QueryString["streamingid"]);
            }
            catch (Exception ex)
            {
                string response = "Error while establishing the connection "; //SessionManager.ConstructJSON(true, "Error while establishing the connection " + ex.Message);
                Console.WriteLine("Exception in WEBSOCKET");
                Send(response);
            }
        }

        private void queuePull(object sender, ElapsedEventArgs e) //_streamingId
        {
            FrameContents fc = null;
            bool dequeueStatus = Dequeue(StreamQueueManager.StreamingIdentifier, ref fc);

            if (dequeueStatus)
            {
                if (fc != null && fc.base64Image != null)
                    Send(fc.base64Image);
            }
        }

        public enum playbackActions
        {
            PLAY,           //0
            STOP,           //1
            PAUSE,          //2
            FORWARD,        //3
            STEPFORWARD,    //4
            FASTFORWARD,    //5
            BACKWARD,       //6
            STEPBACKWARD,   //7
            FASTBACKWARD,   //8
            PAN,            //9
            TILT,           //10
            ZOOM,           //11
            PRESET          //12
        };
    }

    public class FrameContents
    {
        public byte[] pBuf;
        public string base64Image;
        public int nSize;
        public int nHeight;
        public int nWidth;
        public int nframeRate;
        public uint nframeNo;
        public DateTime frameReceiveTime;

        public int queueSize;
        public FrameContents(byte[] _pBuf, int _nSize, int _nHeight, int _nWidth, int _frameRate, uint _frameNo, DateTime _frameReceiveTime)
        {
            pBuf = _pBuf;
            nSize = _nSize;
            nHeight = _nHeight;
            nWidth = _nWidth;
            nframeRate = _frameRate;
            nframeNo = _frameNo;
            frameReceiveTime = _frameReceiveTime;
            base64Image = null;
        }
    }
}
