﻿using MSLocalModeLite.Objects;
using System;
using System.Web.Http;
using WebSocketSharp.Server;
using System.Configuration;
using System.Collections.Specialized;
using HttpServer = WebSocketSharp.Server.HttpServer;
using VideoOS.Platform;
using System.Linq;

namespace MSLocalModeLite
{
    class Program
    {
        private static readonly Guid IntegrationId = new Guid("A67884C7-4088-4E56-8C14-B53A69865B1A");
        private const string IntegrationName = "Status Demo Console";
        private const string Version = "1.0";
        private const string ManufacturerName = "Sample Manufacturer";
        private static int port = 10005;
        


        static void Main(string[] args)
        {
            HttpServer httpsv;
            RequestParameters requestParam = new RequestParameters();
            try
            {   
                httpsv = new HttpServer(port);

                requestParam.DeviceIP = args[0];

                requestParam.DeviceUser = args[1];
                requestParam.DevicePassword = args[2];
                requestParam.DeviceHttpPort = (args[3] != "null") ? args[3] : "";

                string cameraIdentifier = args[4];

                string httpsEnabled_val = args[5];
                requestParam.HttpsEnabled = (httpsEnabled_val == "true") ? true : false;
                

                string windowsAuth_val = args[6];
                requestParam.WindowsAuth = (windowsAuth_val == "true") ? true : false;
                string SocketPort = args[7];
                string startTime = args[8];
                string endTime = args[9];
                bool isForward;
                bool.TryParse(args[10], out isForward);
                bool isFastSeq;
                bool.TryParse(args[11], out isFastSeq);
                double speed = 0;
                double.TryParse(args[12], out speed);
              
                httpsv.AddWebSocketService<Playback>("/streaming");
                httpsv.Start();

                long longEndDateTime;
                long longStartTime; Guid cameraId;
                if ((!Guid.TryParse(cameraIdentifier, out cameraId)) || (!long.TryParse(startTime, out longStartTime)))
                {
                    Console.WriteLine("Invalid Command");
                }
                else 
                {
                    DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                    longStartTime = longStartTime - 8000;
                    DateTime  _startTime = dtDateTime.AddMilliseconds(longStartTime).ToLocalTime();
                    DateTime _endTime;
                    if (!string.IsNullOrEmpty(endTime) && (long.TryParse(endTime, out longEndDateTime)))
                    {
                        _endTime = dtDateTime.AddMilliseconds(longEndDateTime).ToLocalTime();
                    }
                    else
                    {
                        _endTime = _startTime.AddMinutes(5);
                    }
                    
                    requestParam.StartTime = _startTime;
                    requestParam.EndTime = _endTime;
                    requestParam.CameraId = cameraId;
                    requestParam.isPlay = true;
                    requestParam.isForward = isForward;
                    requestParam.isFastseq = isFastSeq;
                    requestParam.Speed = speed;
                    StreamQueueManager.StreamingIdentifier = RandomString(10);                    

                    Playback pl = new Playback();
                    pl.StreamPlayback(requestParam);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Could not logon to management server: " + ex.Message);
            }
            Console.WriteLine("Press any key to exit");
            Console.ReadLine();
        }

        

        public static string RandomString(int length)
        {
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
  
}
