﻿using System;
using System.Collections.Generic;
using VideoOS.Platform;

namespace MSLocalModeLite
{
    public class UserDefinedTriggers
    {
        static List<Item> allTriggerList;
        static List<Trigger> triggerList;
        internal static object TriggerInfo(Item serverItem)
        {
            allTriggerList = new List<Item>();
            triggerList = new List<Trigger>();
            List<Item> serverItems = serverItem.GetChildren();
            Item triggerItem = null;
            foreach (Item item in serverItems)
            {
                if (item.FQID.Kind == Kind.TriggerEvent && item.FQID.ServerId.ServerType == ServerId.CorporateManagementServerType)
                {
                    triggerItem = item;
                }
            }

            if (triggerItem != null)
            {
                List<Item> triggerEventList = triggerItem.GetChildren();
                Dictionary<Guid, Item> allTriggers = new Dictionary<Guid, Item>();
                FindAllUserDefinedTriggers(triggerEventList, allTriggers);
            }
            return triggerList;
        }

        private static List<Trigger> FindAllUserDefinedTriggers(List<Item> items, Dictionary<Guid, Item> triggers)
        {
            foreach (Item item in items)
            {
                if (item.FQID.Kind == Kind.TriggerEvent && item.FQID.FolderType == FolderType.No)
                {
                    if (triggers.ContainsKey(item.FQID.ObjectId) == false)
                    {
                        triggers[item.FQID.ObjectId] = item;
                        Trigger obj = new Trigger()
                        {
                            name = item.Name,
                            id = item.FQID.ObjectId,
                        };
                        triggerList.Add(obj);
                        allTriggerList.Add(item);
                    }
                }
            }
            return triggerList;
        }

        internal static Item GetSelectedTrigger(Guid triggerId, string triggerName, Item serverItem)
        {
            if (triggerList != null && triggerList.Count > 0)
            {
                if (Guid.Empty != triggerId)
                {
                    return allTriggerList.Find(tl => tl.FQID.ObjectId == triggerId);
                }
                else if (!string.IsNullOrEmpty(triggerName))
                {
                    return allTriggerList.Find(tl => tl.Name == triggerName);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                TriggerInfo(serverItem);
                return GetSelectedTrigger(triggerId, triggerName, serverItem);
            }
        }

        internal bool TriggerAction(Item serverItem)
        {
            EnvironmentManager.Instance.SendMessage(
                      new VideoOS.Platform.Messaging.Message(
                          VideoOS.Platform.Messaging.MessageId.Control.TriggerCommand
                          ), serverItem.FQID);
            return true;
        }

        public class Trigger
        {
            public string name;
            public Guid id;
        }

    }
}
