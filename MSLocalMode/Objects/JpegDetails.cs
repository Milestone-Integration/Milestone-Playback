﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSLocalModeLite.Objects
{
    public class JpegDetails
    {

        public JpegDetails(Byte[] bytes, DateTime prevDateTime, DateTime currentDateTime)
        {
            this.FB = bytes;
            this.Delay = (currentDateTime - prevDateTime).TotalMilliseconds;
            this.FrameDT = currentDateTime;
        }

        public JpegDetails(Byte[] bytes)
        {
            this.FB = bytes;
            this.Delay = 0;
        }

        public Byte[] FB { get; set; }
        public double Delay { get; set; }
        public DateTime FrameDT { get; set; }

    }
}
