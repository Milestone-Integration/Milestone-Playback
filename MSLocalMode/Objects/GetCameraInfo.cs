﻿using System;
using System.Collections.Generic;
using System.Linq;
using VideoOS.Platform;
using Newtonsoft.Json.Linq;

namespace MSLocalModeLite.Objects
{
    public class GetCameraInfo : UserDefinedTriggers
    {
        static List<CameraList> cameraList;
        static List<Item> allCameraList;
        static int channelId = 0;
        static List<Item> allTriggerList;
        static List<Trigger> triggerList;
        static DeviceInfo deviceInfo;
        internal static object CameraInfo(Item serverItem)
        {
            channelId = 0;
            cameraList = new List<CameraList>();
            allCameraList = new List<Item>();
            allTriggerList = new List<Item>();
            triggerList = new List<Trigger>();
            deviceInfo = new DeviceInfo();
            if (serverItem != null)
            {

                List<Item> serverItems = serverItem.GetChildren();
                Item recorder = null;
                Item triggerItem = null;
                foreach (Item item in serverItems)
                {
                    if (item.FQID.Kind == Kind.Server && item.FQID.ServerId.ServerType == ServerId.CorporateRecordingServerType)
                    {
                        recorder = item;
                    }

                    if (item.FQID.Kind == Kind.TriggerEvent && item.FQID.ServerId.ServerType == ServerId.CorporateManagementServerType)
                    {
                        triggerItem = item;
                    }
                }

                dynamic deviceObj = new JObject();
                var properties = recorder.Properties.ToList();
                deviceObj.macAddress = properties.Find(x => x.Key == "MacAddress").Value;
                deviceObj.model = properties.Find(x => x.Key == "ProductName").Value;
                deviceObj.serialNumber = properties.Find(x => x.Key == "Guid").Value;
                deviceObj.fwVersion = properties.Find(x => x.Key == "ProductVersion").Value;

                List<Item> allItems = recorder.GetChildren();
                Dictionary<Guid, Item> allCameras = new Dictionary<Guid, Item>();
                if (triggerItem != null)
                {
                    List<Item> triggerEventList = triggerItem.GetChildren();
                    Dictionary<Guid, Item> allTriggers = new Dictionary<Guid, Item>();
                    deviceInfo.IO = new IO() { output = FindAllUserDefinedTriggers(triggerEventList, allTriggers) };
                }

                deviceInfo.CameraList = FindAllCameras(allItems, recorder.FQID.ServerId.Id, allCameras);
                deviceInfo.DeviceProperties = deviceObj;
            }
            else { deviceInfo = null; }
            return deviceInfo;
        }
        private static List<CameraList> FindAllCameras(List<Item> items, Guid recorderGuid, Dictionary<Guid, Item> allCameras)
        {
            foreach (Item item in items)
            {
                if (item.FQID.Kind == Kind.Camera && item.FQID.ParentId == recorderGuid && item.FQID.FolderType == FolderType.No)
                {
                    if (allCameras.ContainsKey(item.FQID.ObjectId) == false)
                    {
                        allCameras[item.FQID.ObjectId] = item;
                        //var channelId = "0";
                        //item.Properties.TryGetValue("Channel", out channelId);
                        CameraList obj = new CameraList()
                        {
                            enabled = item.Enabled == true ? 1 : 0,
                            name = item.Name,
                            channelId = channelId,
                            sensorID = item.FQID.ObjectId,
                            sensorType = "Camera"
                        };
                        cameraList.Add(obj);
                        allCameraList.Add(item);
                        channelId++;
                    }
                }
                else
                    if (item.FQID.FolderType != FolderType.No)
                {
                    FindAllCameras(item.GetChildren(), recorderGuid, allCameras);
                }
            }
            return cameraList;
        }

        private static List<Trigger> FindAllUserDefinedTriggers(List<Item> items, Dictionary<Guid, Item> triggers)
        {
            foreach (Item item in items)
            {
                if (item.FQID.Kind == Kind.TriggerEvent && item.FQID.FolderType == FolderType.No)
                {
                    if (triggers.ContainsKey(item.FQID.ObjectId) == false)
                    {
                        triggers[item.FQID.ObjectId] = item;
                        Trigger obj = new Trigger()
                        {
                            name = item.Name,
                            id = item.FQID.ObjectId,
                        };
                        triggerList.Add(obj);
                        allTriggerList.Add(item);
                    }
                }
            }
            return triggerList;
        }

        internal static Item GetSelectedCamera(Guid cameraNo, Item serverItem)
        {
            if (allCameraList != null && allCameraList.Count > 0)
            {
                //foreach (var camera in allCameraList)
                //{
                //    foreach (var properties in camera.Properties)
                //    {
                //        if (properties.Key == "Channel" && properties.Value == cameraNo.ToString())
                //        {
                //            return camera;
                //        }
                //    }
                //}
                return allCameraList.Find(c => c.FQID.ObjectId == cameraNo);
                //  return null;
            }
            else
            {
                CameraInfo(serverItem);
                return GetSelectedCamera(cameraNo, serverItem);
            }
        }
        internal class DeviceInfo
        {
            public List<CameraList> CameraList { get; set; }
            public object DeviceProperties { get; set; }
            public IO IO { get; set; }
        }
        internal class IO
        {
            public List<Trigger> input { get; set; }
            public List<Trigger> output { get; set; }
        }
    }
}
