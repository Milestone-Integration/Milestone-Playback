﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSLocalModeLite.Objects
{
    public class CameraList
    {
        public string name;
        public int enabled;
        public int channelId;
        public string sensorType;
        public Guid sensorID;
    }
}
