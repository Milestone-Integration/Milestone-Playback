﻿using VideoOS.Platform;

namespace MSLocalModeLite.Objects
{
    public class ClientResponse
    {
        public ClientResponse()
        {
            this.isSuccess = false;
            this.message = "Something went wrong";
            this.data = null;
        }
        public bool isSuccess;
        public string message;
        public Item data;
    }
}
