﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSLocalModeLite.Objects
{
    public class ConnectionState
    {
        public bool isConnected { get; set; }
        public string message { get; set; }
    }
}
