﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MSLocalModeLite
{
   public class RequestParameters
    {
        //public string StreamIdentifier { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public Guid CameraId { get; set; }
        public string DeviceUser { get; set; }
        public string DevicePassword { get; set; }
        public string DeviceHttpPort { get; set; }
        public bool? HttpsEnabled { get; set; }
        public string DeviceIP { get; set; }
        public bool? WindowsAuth { get; set; }

        public bool isForward { get; set; }
        public bool isPlay { get; set; }
        public bool isFastseq { get; set; }
        public double Speed { get; set; }

        //public bool isFastForward { get; set; }


    }
}
